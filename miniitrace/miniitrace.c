/*
 * This is a tiny ptrace-based instruction tracer that can also spit out register dumps.
 * It uses libcapstone to print disassembly, but you can also build it without.
 */

#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/syscall.h>
#include <sys/uio.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <unistd.h>

#ifndef MINIITRACE_NO_CAPSTONE
#include <capstone/capstone.h>
#endif


/* ************************************ UTILITIES ************************************ */

#define __stringify(arg) #arg
#define stringify(arg) __stringify(arg)
#define die(fmt, ...) do { dprintf(STDERR_FILENO, __FILE__ ":" stringify(__LINE__) " | " fmt "\n", ##__VA_ARGS__); exit(1); } while (0)


/* ************************************ OPTIONS AND OUTPUT FILES ************************************ */

struct options {
    /* Actual options */
    const char *output_path;
    bool follow_fork;
    bool split_outputs;
    bool show_disassembly;
    bool show_registers;

    /* Internal stuff */
    int output_fd;
    struct {
        size_t capacity;
        size_t used;
        struct { int pid, fd; } *mapping;
    } split_fds;
};

int output_fd_for_pid(struct options *opts, int pid)
{
    if (!opts->split_outputs)
        return opts->output_fd;

    /*
     * Look up the file descriptor for this PID.
     * This probably deserves a hash map or at least a sorted array with binary search, but I'd
     * rather keep this small (how often are we stracing massive multi-process things?).
     * Also note that since we don't do any cleanup handling, the same file may be reused if the PID
     * is reused (this shouldn't normally happen, but I figured I'd mention it here).
     */
    for (size_t i = 0; i < opts->split_fds.used; ++i)
        if (opts->split_fds.mapping[i].pid == pid)
            return opts->split_fds.mapping[i].fd;

    /* Never seen this PID before, open a new file. */
    char path[PATH_MAX];
    if (snprintf(path, sizeof(path), "%s.%d", opts->output_path, pid) >= (int) sizeof(path))
        die("Failed to generate output file name: %s", strerror(ENAMETOOLONG));

    int fd;
    if ((fd = open(path, O_WRONLY | O_CREAT | O_APPEND, 0644)) == -1)
        die("Failed to open output file '%s': %m", path);

    size_t capacity = opts->split_fds.capacity;
    size_t index = opts->split_fds.used++;
    if (index >= capacity) {
        /* Must resize the array. */
        capacity = capacity ? capacity * 2 : 16;
        void *reallocated = reallocarray(
            opts->split_fds.mapping, /* This may be NULL, but that's OK. */
            capacity,
            sizeof(opts->split_fds.mapping[0])
        );
        if (!reallocated)
            die("Failed to allocate memory");
        opts->split_fds.mapping = reallocated;
    }
    opts->split_fds.mapping[index].pid = pid;
    opts->split_fds.mapping[index].fd = fd;

    return fd;
}


/* ************************************ ARCHITECTURE-SPECIFIC STUFF ************************************ */

#if defined(__x86_64__)
#define ARCH_SELECT(x86_64, ...) x86_64
#else
#error Please extend the per-architecture definitions!
#endif

#if defined(__x86_64__)
int x86_show_gpregs(struct user_regs_struct *regs, char *buffer, size_t size)
{
    return snprintf(buffer, size,
         "    rax %#-18jx    rbx %#-18jx\n"
         "    rcx %#-18jx    rdx %#-18jx\n"
         "    rsi %#-18jx    rdi %#-18jx\n"
         "    rbp %#-18jx    rsp %#-18jx\n"
         "    r8  %#-18jx    r9  %#-18jx\n"
         "    r10 %#-18jx    r11 %#-18jx\n"
         "    r12 %#-18jx    r13 %#-18jx\n"
         "    r14 %#-18jx    r15 %#-18jx\n"
         "    rip %#-18jx    fl  %#-18jx\n",
         (uintmax_t) regs->rax, (uintmax_t) regs->rbx,
         (uintmax_t) regs->rcx, (uintmax_t) regs->rdx,
         (uintmax_t) regs->rsi, (uintmax_t) regs->rdi,
         (uintmax_t) regs->rbp, (uintmax_t) regs->rsp,
         (uintmax_t) regs->r8,  (uintmax_t) regs->r9,
         (uintmax_t) regs->r10, (uintmax_t) regs->r11,
         (uintmax_t) regs->r12, (uintmax_t) regs->r13,
         (uintmax_t) regs->r14, (uintmax_t) regs->r15,
         (uintmax_t) regs->rip, (uintmax_t) regs->eflags
    );
}
#endif


/* ************************************ THE TRACEE ************************************ */

static void child_tracee(char *argv[], char *envp[])
{
    /* Die on parent exit. NB: This is insufficient to prevent process leaks on its own. */
    prctl(PR_SET_PDEATHSIG, SIGKILL, 0, 0, 0);

    /* Allow our parent to trace us */
    prctl(PR_SET_PTRACER, getppid(), 0, 0, 0);

    /* Tell the parent that we are ready, and wait for it to attach. */
    raise(SIGSTOP);

    /* Execute the child. */
    execve(argv[0], argv, envp);
    die("Failed to launch %s: %m", argv[0]);
}


/* ************************************ THE TRACER ************************************ */

static int get_signal(int child_pid, int status)
{
    if (WSTOPSIG(status) != SIGTRAP)
        return WSTOPSIG(status);

    /* SIGTRAP could be a ptrace event, or it could be something else */
    if (status >> 16)
        return 0; /* PTRACE_EVENT_* */

    /* This could still be a normal single-step, we have to check siginfo */
    siginfo_t siginfo;
    if (ptrace(PTRACE_GETSIGINFO, child_pid, 0, &siginfo))
        die("Failed to get signal info: %m");
    return (siginfo.si_code <= 0 || siginfo.si_code == SI_KERNEL) ? SIGTRAP /* Non-ptrace SIGTRAP */ : 0;
}

static void parent_tracer(int root_pid, struct options *opts)
{
    /* Wait for the child process. This is inherently racy - the raise(SIGSTOP) may have gone through already */
    int status;
    int live = 1;
    if (waitpid(root_pid, &status, __WALL | WUNTRACED | WSTOPPED) != root_pid)
        die("Failed to wait for child process: %m");
    if (!WIFSTOPPED(status) || WSTOPSIG(status) != SIGSTOP)
        die("Child process is not suspended on SIGSTOP (status is %#x)", status);

    /* Seize the child process */
    if (ptrace(PTRACE_SEIZE, root_pid, NULL, PTRACE_O_EXITKILL | PTRACE_O_TRACECLONE | PTRACE_O_TRACEEXEC | PTRACE_O_TRACEFORK | PTRACE_O_TRACESYSGOOD | PTRACE_O_TRACEVFORK))
        die("Failed to attach to child process: %m");

    /* Continue up to the execve */
    if (ptrace(PTRACE_CONT, root_pid, NULL, (void *) SIGCONT))
        die("Failed to continue child process: %m");
    if (waitpid(root_pid, &status, 0) != root_pid)
        die("Failed to obtain child process status: %m");
    if (!WIFSTOPPED(status) || WSTOPSIG(status) != SIGTRAP || (status >> 16) != PTRACE_EVENT_EXEC)
        die("Child is not suspended on execve (status is %#x)", status);

    /* Now single-step. */
    if (ptrace(PTRACE_SINGLESTEP, root_pid, NULL, NULL))
        die("Failed to single-step child process: %m");

#ifndef MINIITRACE_NO_CAPSTONE
    csh handle;
    cs_insn *insn;

    int error = cs_open(ARCH_SELECT(CS_ARCH_X86), ARCH_SELECT(CS_MODE_64), &handle);
    if (error != CS_ERR_OK)
        die("Failed to create disassembler (error is %d)", error);
#endif

    while (live > 0) {
        /* Grab a random child that just stepped */
        int child_pid = waitpid(-1, &status, __WALL);
        if (child_pid < 0)
            die("Failed to obtain child process status: %m");

        int output_fd = output_fd_for_pid(opts, child_pid);

        unsigned long event;
        if (WIFEXITED(status)) {
            /* Child exited. */
            event = WEXITSTATUS(status);
            goto exited;
        } else if (WIFSIGNALED(status)) {
            /* Child died from a signal */
            --live;
            dprintf(output_fd, "[pid %d terminated with signal %d]\n", child_pid, WTERMSIG(status));
            continue;
        } else if (!WIFSTOPPED(status)) {
            /* Child is not stopped at all. This shouldn't really happen. */
            die("Child process stopped in unexpected state (status is %#x)", status);
        }

        /* Child is now stopped. Detect if we need to deliver a signal. */
        long deliver_signal = get_signal(child_pid, status);
        if (deliver_signal)
            dprintf(output_fd, "[delivering signal %ld to pid %d]\n", deliver_signal, child_pid);

        /* If we have a special event, handle it */
        if (status >> 16) {
            if (ptrace(PTRACE_GETEVENTMSG, child_pid, NULL, &event))
                die("Failed to get event message: %m");

            switch (status >> 16) {
                case PTRACE_EVENT_STOP:
                    /* This is ptrace group-stop, ignore it for now. */
                    break;

                case PTRACE_EVENT_EXEC:
                    /* Log the execve */
                    dprintf(output_fd, "[pid %d executing a new program]\n", child_pid);
                    break;
                case PTRACE_EVENT_CLONE:
                case PTRACE_EVENT_FORK:
                case PTRACE_EVENT_VFORK:
                    /* We want to trace that process too, unless we are not in follow-fork mode */
                    dprintf(output_fd, "[pid %d spawned a new process with pid %lu]\n", child_pid, event);
                    if (opts->follow_fork)
                        ++live;
                    else if (ptrace(PTRACE_DETACH, event, NULL, NULL))
                        die("Failed to detach from child process %lu", event);
                    break;

                case PTRACE_EVENT_EXIT:
                exited:
                    --live;
                    dprintf(output_fd, "[pid %d terminated with exit code %ld]\n", child_pid, (long) event);
                    continue;
            }
        }

        /* Grab register state */
        ARCH_SELECT(struct user_regs_struct) regs;
        if (ptrace(PTRACE_GETREGS, child_pid, NULL, &regs))
            die("Failed to retrieve child registers: %m");
        unsigned long ip = ARCH_SELECT(regs.rip);

        /* Start building the message */
        char message[4096];
        int position = 0;

        /* Show registers first - they have the values from _before_ the instruction at the current pc is executed */
        if (opts->show_registers) {
            position += ARCH_SELECT(x86_show_gpregs)(&regs, message + position, sizeof(message) - position - 1);
            /* TODO: Do we also want to show fpregs? */
        }

        /* Now log the instruction pointer and disassemble the instruction if requested */
        if (opts->follow_fork && !opts->split_outputs)
            position += snprintf(message + position, sizeof(message) - position - 1, "[pid %d] ", child_pid);
        position += snprintf(message + position, sizeof(message) - position - 1, "%#lx", ip);

#ifndef MINIITRACE_NO_CAPSTONE
        if (opts->show_disassembly) {
            uint8_t code[ARCH_SELECT(16)];
            size_t to_copy = sizeof(code);

            do {
                struct iovec local = { .iov_base = code, .iov_len = to_copy };
                struct iovec remote = { .iov_base = (void *) ip, .iov_len = to_copy };

                ssize_t copied = process_vm_readv(child_pid, &local, 1, &remote, 1, 0);
                if (copied > 0 && (size_t) copied == to_copy) {
                    size_t count = cs_disasm(handle, code, to_copy, ip, 1, &insn);
                    if (count == 0)
                        position += snprintf(message + position, sizeof(message) - position - 1, " \t[invalid]\n");
                    else
                        position += snprintf(message + position, sizeof(message) - position - 1, " \t%s\t%s\n", insn->mnemonic, insn->op_str);
                    cs_free(insn, count);
                    break;
                } else if (errno != EFAULT)
                    die("Failed to read code from child process %d at %#lx: %m", child_pid, ip);
                else if (!--to_copy)
                    position += snprintf(message + position, sizeof(message) - position - 1, " \t[unmapped]\n");
            } while (to_copy);
        } else
#endif
            snprintf(message + position, sizeof(message) - position - 1, "\n");

        if (ptrace(PTRACE_SINGLESTEP, child_pid, NULL, (void *) deliver_signal))
            die("Failed to step child process %d: %m", child_pid);

        dprintf(output_fd, "%s", message);
    }
}


/* ************************************ INITIALIZATION AND SETUP ************************************ */

#define TTY_HIGHLIGHT(text) "\x1b[31;1m" text "\x1b[0m"
#define TTY_ARG(text) "\x1b[32;1m" text "\x1b[0m"
#define NO_COLOR(text) text
#define SELECT_TTY(is_tty, macro) ((is_tty) ? macro(TTY_HIGHLIGHT, TTY_ARG) : macro(NO_COLOR, NO_COLOR))

#define USAGE_STRING(hl, arg) \
    hl("USAGE") "\n" \
    "    " hl("miniitrace") " [" hl("-hdfr") "] [" hl("-o") " " arg("path") "] [--] " arg("command") " [" arg("args") "]\n" \
    "\n" \
    hl("OPTIONS") "\n" \
    "    " hl("-h") "  " hl("--help") "         Print this help message\n" \
    "    " hl("-d") "  " hl("--disassemble") "  Show disassembly\n" \
    "    " hl("-f") "  " hl("--follow-fork") "  Trace forked processes also\n" \
    "    " hl("-r") "  " hl("--registers") "    Show register values\n" \
    "    " hl("-o") "  " hl("--output") "       Write output to the specified " arg("path") "\n" \
    "    " hl("-s") "  " hl("--split") "        Generate one output file per process, if " hl("-o") " is specified\n" \

int usage(int error)
{
    bool tty = isatty(STDOUT_FILENO);
    puts(SELECT_TTY(tty, USAGE_STRING));
    return error;
}

#define option(arg, short, long) (((short) && strcmp((arg), (short)) == 0) || ((long) && strcmp((arg), (long)) == 0))
#define store_value(into, argv, argi) \
    do { \
        if (!argv[argi + 1]) { dprintf(STDERR_FILENO, "Missing argument for option '%s'\n", argv[argi]); return 1; } \
        into = argv[++argi]; \
    } while (0)
#define option_error(message, ...) (dprintf(STDERR_FILENO, message "\n", ##__VA_ARGS__), 1)

int main(int argc, char *argv[], char *envp[])
{
    /* Parse arguments */
    if (argc <= 1)
        return usage(1);

    int argi = 1;
    struct options opts;
    memset(&opts, 0, sizeof(opts));
    opts.output_fd = STDOUT_FILENO;

    for (; argi < argc; ++argi) {
        if (argv[argi][0] != '-' || strcmp(argv[argi], "--") == 0)
            break; /* End of options at -- or any non-prefixed argument */
        else if (option(argv[argi], "-h", "--help"))
            return usage(0);
        else if (option(argv[argi], "-d", "--disassemble"))
            opts.show_disassembly = true;
        else if (option(argv[argi], "-f", "--follow-fork")) /* NB: CLONE_UNTRACED is not caught by this (and inspecting clone3 is inherently race-prone) */
            opts.follow_fork = true;
        else if (option(argv[argi], "-s", "--split"))
            opts.split_outputs = true;
        else if (option(argv[argi], "-r", "--registers"))
            opts.show_registers = true;
        else if (option(argv[argi], "-o", "--output"))
            store_value(opts.output_path, argv, argi);
        else
            return option_error("Unknown option: '%s'", argv[argi]);
    }

#ifdef MINIITRACE_NO_CAPSTONE
    if (opts.show_disassembly)
        die("-d/--disassemble is not supported without capstone");
#endif

    if (opts.split_outputs && !opts.output_path)
        die("-s/--split is not supported without -o/--output");
    if (opts.output_path && !opts.split_outputs)
        if ((opts.output_fd = open(opts.output_path, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
            die("Failed to open output file: %m");

    /* Spawn the child */
    int root_pid = fork();
    if (root_pid == -1)
        die("Failed to fork child process: %m");

    /* Dispatch */
    if (root_pid == 0)
        child_tracee(&argv[argi], envp);
    else
        parent_tracer(root_pid, &opts);
}
