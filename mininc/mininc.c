#include <arpa/inet.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

#define STR_HELPER(arg) #arg
#define STR(arg) STR_HELPER(arg)
#define PERROR(msg, ...) do { if (__VA_ARGS__) { perror(msg); exit(1); } } while (0)
#define ASSERT(...) do { if (!(__VA_ARGS__)) { fprintf(stderr, "%s:%d:\tAssertion failed: %s\n", __FILE__, __LINE__, STR(__VA_ARGS__)); exit(1); } } while (0)

#if (!defined IP_VERSION)
#define IP_VERSION 4
#endif
#if (!defined TIMEOUT_MS)
#define TIMEOUT_MS 10000
#endif
#if (!defined BUFFER_SIZE)
#define BUFFER_SIZE 2048
#endif

#if (IP_VERSION == 4)
#define FAMILY AF_INET
#define ADDR_TYPE sockaddr_in
#elif (IP_VERSION == 6)
#define FAMILY AF_INET6
#define ADDR_TYPE sockaddr_in6
#else
#error "Invalid value for IP_VERSION (must be 4 or 6, default is 4)"
#endif

#define read_oob(fd, buf, sz) recv((fd), (buf), (sz), MSG_OOB)
#define TRANSFER(read_fn, in_fd, write_fn, out_fd, buffer, size, outsz) do { \
	ssize_t bytes = read_fn((in_fd), (buffer), (size)); \
	PERROR(STR(read_fn), bytes < 0); \
	outsz = bytes; \
	for (int offset = 0; bytes > 0;) { \
		ssize_t sent = write_fn((out_fd), (buffer) + offset, (size_t) bytes); \
		PERROR(STR(write_fn), sent <= 0); \
		bytes -= sent; \
		offset += sent; \
	} \
} while (0)

int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr, "Usage:\n\t%s <ip> <port>\n", argv[0]);
		return 1;
	}
	int result;

	PERROR("setvbuf", setvbuf(stdin, NULL, _IONBF, 0));
	PERROR("setvbuf", setvbuf(stdout, NULL, _IONBF, 0));

	int port = atoi(argv[2]);
	ASSERT(0 < port && port <= 65535);

	struct ADDR_TYPE addr = {0};
	addr.sin_family = FAMILY;
	addr.sin_port = htons(port);

	result = inet_pton(FAMILY, argv[1], &addr.sin_addr);
	PERROR("inet_pton", result < 0);
	if (!result) {
		fprintf(stderr, "%s is not a valid IPv%d address\n", argv[1], IP_VERSION);
		return 1;
	}

	int fd = socket(FAMILY, SOCK_STREAM, 0);
	PERROR("socket", fd < 0);
	PERROR("connect", connect(fd, (struct sockaddr *) &addr, sizeof(struct ADDR_TYPE)));

	int do_poll_stdin = 1;
	struct pollfd fds[2];
	fds[0].fd = fd;
	fds[0].events = POLLIN | POLLPRI;
	fds[1].fd = STDIN_FILENO;
	fds[1].events = POLLIN;

	char buffer[BUFFER_SIZE];
	ssize_t transferred;
	for (;;) {
		fds[0].revents = fds[1].revents = 0;
		result = poll(fds, do_poll_stdin ? 2 : 1, TIMEOUT_MS);
		PERROR("poll", result < 0);
		if (!result) {
			fprintf(stderr, "Connection timed out.\n");
			return 1;
		}

		transferred = 0;
		if (fds[0].revents & POLLPRI)
			TRANSFER(read_oob, fd, write, STDOUT_FILENO, buffer, BUFFER_SIZE, transferred);
		else if (fds[0].revents & POLLIN)
			TRANSFER(read, fd, write, STDOUT_FILENO, buffer, BUFFER_SIZE, transferred);
		if ((fds[0].revents && !transferred) || (fds[0].revents & POLLHUP))
			break;

		transferred = 0;
		if (fds[1].revents & POLLIN)
			TRANSFER(read, STDIN_FILENO, write, fd, buffer, BUFFER_SIZE, transferred);
		if ((fds[1].revents && !transferred) || (fds[1].revents & POLLHUP))
			PERROR("shutdown", (do_poll_stdin = shutdown(fd, SHUT_WR)));
	}

	shutdown(fd, SHUT_RDWR);
	PERROR("close", close(fd));
	return 0;
}
