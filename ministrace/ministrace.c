/*
 * This is a tiny strace that should build statically with either musl or glibc.
 * Of course, we have nowhere close to the feature set of the standard strace.
 * This strace uses seccomp_unotify(2) rather than ptrace(2). This has a few advantages:
 *  - seccomp filters are automatically inherited, so we don't need to fight against CLONE_UNTRACED
 *  - ptrace(2) actually works in the targeted processes
 * There are also disadvantages, of course:
 *  - We can't see syscall results or incoming signals.
 *  - We don't currently support x32 (compatibility) system calls, or switches to 32-bit mode.
 *  - We have to set PR_SET_NO_NEW_PRIVS to install our filter, unless we have CAP_SYS_ADMIN.
 *  - We can't attach to running processes.
 */

#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <linux/audit.h>
#include <linux/capability.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <limits.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <unistd.h>

#ifndef SECCOMP_RET_USER_NOTIF
#include "compat.h" /* musl is missing quite some stuff */
#endif


/* ************************************ PER-ARCHITECTURE STUFF ************************************ */

#if defined(__x86_64__)
#define AUDIT_ARCH_CURRENT AUDIT_ARCH_X86_64
#define busy_wait() __asm__ volatile ("pause"); /* This can just be do {} while (0) if you don't want to be nice to the CPU */
#else
#error Please extend the per-architecture definitions!
#endif


/* ************************************ UTILITIES ************************************ */

#define __stringify(arg) #arg
#define stringify(arg) __stringify(arg)
#define __concat(a, b) a##b
#define concat(a, b) __concat(a, b)
#define array_size(arr) (sizeof(arr) / sizeof(arr[0]))
#define die(fmt, ...) do { dprintf(STDERR_FILENO, __FILE__ ":" stringify(__LINE__) " | " fmt "\n", ##__VA_ARGS__); exit(1); } while (0)

int reaper_original_child = 0;

void reap_child(int signo)
{
    (void) signo;
    for (int pid; (pid = waitpid((pid_t) -1, NULL, WNOHANG)) > 0;) {
        printf("[pid %d] exited.\n", pid);
        if (pid == reaper_original_child)
            exit(0);
    }
}


/* ************************************ OPTIONS AND OUTPUT FILES ************************************ */

struct options {
    /* Actual options */
    const char *output_path;
    const char *command_hook;
    bool split_outputs;
    bool drop_capabilities;
    bool show_internal;

    /* File descriptor mappings */
    int output_fd;
    struct {
        size_t capacity;
        size_t used;
        struct { int pid, fd; } *mapping;
    } split_fds;
};

int output_fd_for_pid(struct options *opts, int pid)
{
    if (!opts->split_outputs)
        return opts->output_fd;

    /*
     * Look up the file descriptor for this PID.
     * This probably deserves a hash map or at least a sorted array with binary search, but I'd
     * rather keep this small (how often are we stracing massive multi-process things?).
     * Also note that since we don't do any cleanup handling, the same file may be reused if the PID
     * is reused (this shouldn't normally happen, but I figured I'd mention it here).
     */
    for (size_t i = 0; i < opts->split_fds.used; ++i)
        if (opts->split_fds.mapping[i].pid == pid)
            return opts->split_fds.mapping[i].fd;

    /* Never seen this PID before, open a new file. */
    char path[PATH_MAX];
    if ((size_t) snprintf(path, sizeof(path), "%s.%d", opts->output_path, pid) >= sizeof(path))
        die("Failed to generate output file name: %s", strerror(ENAMETOOLONG));

    int fd;
    if ((fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
        die("Failed to open output file '%s': %m", path);

    size_t capacity = opts->split_fds.capacity;
    size_t index = opts->split_fds.used++;
    if (index >= capacity) {
        /* Must resize the array. */
        capacity = capacity ? capacity * 2 : 16;
        void *reallocated = reallocarray(
            opts->split_fds.mapping, /* This may be NULL, but that's OK. */
            capacity,
            sizeof(opts->split_fds.mapping[0])
        );
        if (!reallocated)
            die("Failed to allocate memory");
        opts->split_fds.mapping = reallocated;
    }
    opts->split_fds.mapping[index].pid = pid;
    opts->split_fds.mapping[index].fd = fd;

    return fd;
}


/* ************************************ THE TRACEE ************************************ */

volatile sig_atomic_t seccomp_is_waiting = 0;

struct sock_filter seccomp_trace_filter[] = {
    BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_USER_NOTIF)
};

static bool capable(uint64_t cap_mask)
{
    struct __user_cap_header_struct header = { .version = _LINUX_CAPABILITY_VERSION_3, .pid = 0 };
    struct __user_cap_data_struct data[2];
    memset(data, 0, sizeof(data));

    if (syscall(__NR_capget, &header, data) == -1)
        die("Failed to get current capability set: %m");

    return ((data[0].effective | ((uint64_t) data[1].effective << 32)) & cap_mask) == cap_mask;
}

static void drop_capabilities(uint64_t cap_mask)
{
    struct __user_cap_header_struct header = { .version = _LINUX_CAPABILITY_VERSION_3, .pid = 0 };
    struct __user_cap_data_struct data[2];
    memset(data, 0, sizeof(data));

    for (size_t i = 0; i < array_size(data); ++i) {
        data[i].permitted &= ~cap_mask;
        data[i].effective &= ~cap_mask;
        data[i].inheritable &= ~cap_mask;
        cap_mask >>= 32;
    }
    if (syscall(__NR_capset, &header, &data[0]))
        die("Failed to drop capabilities: %m");

    for (size_t capability = 0; capability <= CAP_LAST_CAP; ++capability) {
        if (cap_mask & (1 << capability)) {
            if (prctl(PR_CAP_AMBIENT, PR_CAP_AMBIENT_LOWER, capability, 0, 0))
                die("Failed to clear ambient capability set: %m");
            if (prctl(PR_CAPBSET_DROP, capability))
                die("Failed to drop bounding capability %d: %m", capability);
        }
    }
}

static void child_tracee(struct options *opts, char *argv[], char *envp[])
{
    bool cap_sys_admin = capable(1 << CAP_SYS_ADMIN);

    if (!cap_sys_admin) {
        /* Set no-new-privs (needed if we don't have CAP_SYS_ADMIN). This can break things. */
        dprintf(STDERR_FILENO, "Warning: Enabling PR_SET_NO_NEW_PRIVS. Run ministrace with CAP_SYS_ADMIN to disable this message.\n");
        if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0))
            die("Failed to set no-new-privs mode: %m");
    }

    /*
     * Die on parent exit.
     * This is technically insufficient to prevent process leaks,
     * but note that once the seccomp_unotify listener goes away,
     * all system calls will fail with -ENOSYS, which generally leads
     * to a quick end anyways.
     */
    prctl(PR_SET_PDEATHSIG, SIGKILL, 0, 0, 0);

    /* Install the seccomp filter */
    struct sock_fprog trace_filter = { .len = array_size(seccomp_trace_filter), .filter = seccomp_trace_filter };
    int notify_fd = syscall(__NR_seccomp, SECCOMP_SET_MODE_FILTER, SECCOMP_FILTER_FLAG_NEW_LISTENER, &trace_filter);
    if (notify_fd == -1)
        die("Failed to install seccomp filter: %m");
    else if (notify_fd == 0)
        die("File descriptor for seccomp notifications is zero"); /* 0 is special to us, sorry. */

    /* Wait for the parent to attach and steal our seccomp filter file descriptor */
    seccomp_is_waiting = notify_fd;
    while (seccomp_is_waiting)
        busy_wait();

    /* Check for setuid/setgid privileges, and if so, drop them */
    if (opts->drop_capabilities) {
        uid_t real_user, effective_user, saved_user;
        gid_t real_group, effective_group, saved_group;
        if (getresuid(&real_user, &effective_user, &saved_user))
            die("Failed to get user IDs: %m");
        if (getresgid(&real_group, &effective_group, &saved_group))
            die("Failed to get group IDs: %m");
        if (real_group != effective_group || real_user != effective_user)
            if (setgroups(0, NULL))
                die("Failed to drop supplementary groups: %m");
        if (real_group != effective_group)
            if (setresgid(real_group, real_group, real_group))
                die("Failed to switch groups from %d to %d: %m", effective_group, real_group);
        if (real_user != effective_user)
            if (setresuid(real_user, real_user, real_user))
                die("Failed to switch users from %d to %d: %m", effective_user, real_user);
    }

    /* Drop CAP_SYS_ADMIN unless explicitly requested */
    if (cap_sys_admin && opts->drop_capabilities)
        drop_capabilities(1 << CAP_SYS_ADMIN);


    /* Execute the child. */
    execve(argv[0], argv, envp);
    die("Failed to launch %s: %m", argv[0]);
}


/* ************************************ THE TRACER ************************************ */

#include "syscalls.h" /* describe_syscall() is in here */

static void parent_tracer(int child_pid, struct options *opts)
{
    /* Grab notification size */
    struct seccomp_notif_sizes sizes;
    if (syscall(__NR_seccomp, SECCOMP_GET_NOTIF_SIZES, 0, &sizes))
        die("Failed to retrieve seccomp struct sizes: %m");
    if (sizeof(struct seccomp_data) != sizes.seccomp_data)
        die("struct seccomp_data changed size - please recompile this with the correct kernel headers.");
    if (sizeof(struct seccomp_notif) != sizes.seccomp_notif)
        die("struct seccomp_notif changed size - please recompile this with the correct kernel headers.");
    if (sizeof(struct seccomp_notif_resp) != sizes.seccomp_notif_resp)
        die("struct seccomp_notif_resp changed size - please recompile this with the correct kernel headers.");

    /* Grab the seccomp_unotify file descriptor from the tracee */
    int pid_fd = syscall(__NR_pidfd_open, child_pid, 0);
    if (pid_fd == -1)
        die("Failed to open pidfd: %m");

    if (ptrace(PTRACE_SEIZE, child_pid, NULL, NULL))
        die("Failed to attach to child process: %m");

    unsigned long child_fd_nr = 0;
    while (!child_fd_nr) {
        if (ptrace(PTRACE_INTERRUPT, child_pid, NULL, NULL))
            die("Failed to interrupt child process: %m");
        if (waitpid(child_pid, NULL, 0) == -1)
            die("Failed to wait for child process: %m");

        /* Just read the data directly, no "data was -1" shenanigans */
        if (syscall(__NR_ptrace, PTRACE_PEEKDATA, child_pid, &seccomp_is_waiting, &child_fd_nr) == -1)
            die("Failed to read file descriptor number from child: %m");
        child_fd_nr &= __builtin_choose_expr(sizeof(sig_atomic_t) >= 8, 0, (1ull << (sizeof(sig_atomic_t) * 8))) - 1;
        if (child_fd_nr)
            break;

        if (ptrace(PTRACE_CONT, child_pid, NULL, NULL))
            die("Failed to resume child process: %m");
    }

    int notify_fd = syscall(__NR_pidfd_getfd, pid_fd, (int) child_fd_nr, 0);
    if (notify_fd == -1)
        die("Failed to grab notification file descriptor from child: %m");

    if (ptrace(PTRACE_POKEDATA, child_pid, &seccomp_is_waiting, NULL) == -1)
        die("Failed to signal child: %m");
    if (ptrace(PTRACE_DETACH, child_pid, NULL, NULL))
        die("Failed to detach from child process: %m");

    /* SIGCHLD handling */
    reaper_original_child = child_pid;
    signal(SIGCHLD, reap_child);

    /* We are now ready. */
    struct seccomp_notif notification;
    struct seccomp_notif_resp response;
    bool had_initial_execve = opts->show_internal;

    for (;;) {
        /* Fetch the notification */
        memset(&notification, 0, sizeof(notification));
        if (ioctl(notify_fd, SECCOMP_IOCTL_NOTIF_RECV, &notification) == -1) {
            if (errno == EINTR)
                continue;
            die("Failed to retrieve notification: %m");
        }

        int valid = ioctl(notify_fd, SECCOMP_IOCTL_NOTIF_ID_VALID, &notification.id);
        if (valid == -1)
            die("Failed to validate notification: %m");
        if (valid != 0)
            die("Notification %#lx is invalid (%d)", notification.id, valid);

        /* Skip initial syscalls unless requested */
        if (had_initial_execve) {
            /* Get the output file descriptor */
            int fd = output_fd_for_pid(opts, notification.pid);

            /* Check the architecture. It shouldn't be possible to mix architectures in normal operation - x86_64 seems to be the exception. For now, don't support it. */
            if (notification.data.arch != AUDIT_ARCH_CURRENT) {
                describe_unknown_syscall(fd, &notification);
                die("[pid %d] Architecture mismatch: Last system call reported unknown architecture %#x (expected %#x)", notification.pid, notification.data.arch, AUDIT_ARCH_CURRENT);
            }

            /*
             * Handle custom hooks.
             * Hooks are executed as shell commands, with the command pasted directly in front of the
             * arguments (no quoting!). This is useful if you want to pass additional flags, but may
             * require some care to set up properly.
             * In the end, the command is
             *     ./hook pid instruction_pointer syscall_nr args...
             * with all values except PID and syscall number printed as hexadecimal (with the 0x prefix).
             * In bash, you may want to run those through `$(( ${arg} ))` to convert them to decimal.
             * Exit codes have the following meaning:
             *     0: Everything is OK, continue as usual
             *     1: Everything is OK, but please stop executing and quit.
             *     2: Everything is OK, but please do not print this system call.
             * Anything else is treated as an unexpected error, and leads to termination.
             */
            bool should_print = true;
            if (opts->command_hook) {
                /* Yes, I know, system(sprintf(...)) and all that. These are all numbers. */
                char command[4096];
                if ((size_t) snprintf(command, sizeof(command), "%s %d %#lx %d %#lx %#lx %#lx %#lx %#lx %#lx",
                             opts->command_hook, notification.pid, notification.data.instruction_pointer,
                             notification.data.nr, notification.data.args[0], notification.data.args[1],
                             notification.data.args[2], notification.data.args[3], notification.data.args[4],
                             notification.data.args[5]) >= sizeof(command))
                    die("Failed to create hook command: %s", strerror(ENAMETOOLONG));

                /* Run the command and handle the exit code */
                errno = 0;
                int exit_code = system(command);
                if (exit_code == -1 && errno)
                    die("Failed to invoke hook: %m");
                else if (exit_code == 1)
                    die("Hook requested termination (exit code 1).");
                else if (exit_code == 2)
                    should_print = false;
                else if (exit_code)
                    die("Hook terminated with non-zero exit code %d", exit_code);
            }

#if AUDIT_ARCH_CURRENT == AUDIT_ARCH_X86_64
            if (notification.data.nr & __X32_SYSCALL_BIT) {
                /* We can probably print most of these. TODO: Some syscalls may need special handling. */
                notification.data.nr &= ~__X32_SYSCALL_BIT;
            }
#endif

            /* Log the system call */
            if (should_print)
                describe_syscall(fd, &notification);
        } else if (notification.data.nr == __NR_execve || notification.data.nr == __NR_execveat) {
            had_initial_execve = true;
        }

        /* Tell seccomp that it can continue executing the system call */
        response.id = notification.id;
        response.val = 0;
        response.error = 0;
        response.flags = SECCOMP_USER_NOTIF_FLAG_CONTINUE;

        /*
         * TODO: Sanity check that this is fine.
         * We used to block prctl(PR_SET_SECCOMP, ...) and seccomp(...) here,
         * but I don't think it is actually necessary since seccomp picks the most restrictive
         * option by default (see man 2 seccomp).
         */

        if (ioctl(notify_fd, SECCOMP_IOCTL_NOTIF_SEND, &response) == -1)
            die("Failed to respond to notification: %m");
    }
}


/* ************************************ INITIALIZATION AND SETUP ************************************ */

#define TTY_HIGHLIGHT(text) "\x1b[31;1m" text "\x1b[0m"
#define TTY_ARG(text) "\x1b[32;1m" text "\x1b[0m"
#define NO_COLOR(text) text
#define SELECT_TTY(is_tty, macro) ((is_tty) ? macro(TTY_HIGHLIGHT, TTY_ARG) : macro(NO_COLOR, NO_COLOR))

#define USAGE_STRING(hl, arg) \
    hl("USAGE") "\n" \
    "    " hl("ministrace") " [" hl("-hps") "] [" hl("-e") " " arg("command") "] [" hl("-o") " " arg("path") "] [--] " arg("command") " [" arg("args") "]\n" \
    "\n" \
    hl("OPTIONS") "\n" \
    "    " hl("-h") "  " hl("--help") "        Print this help message\n" \
    "    " hl("-p") "  " hl("--privileged") "  Do not drop root privileges / CAP_SYS_ADMIN if started with them\n" \
    "    " hl("-s") "  " hl("--split") "       Generate one output file per process, if " hl("-o") " is specified\n" \
    "    " hl("-e") "  " hl("--exec") "        Execute the " arg("command") " for each system call that is encountered\n" \
    "    " hl("-o") "  " hl("--output") "      Write output to the specified " arg("path") "\n" \

#define ADVANCED_USAGE_STRING(hl, arg) \
    "\n" \
    hl("ADVANCED") "\n" \
    "    "    "  "  "  " hl("--internal") "    Also show system calls made by ministrace before running your command\n" \

int usage(int error, bool advanced)
{
    bool tty = isatty(STDOUT_FILENO);
    puts(SELECT_TTY(tty, USAGE_STRING));
    if (advanced)
        puts(SELECT_TTY(tty, ADVANCED_USAGE_STRING));
    return error;
}

#define option(arg, short, long) (((short) && strcmp((arg), (short)) == 0) || ((long) && strcmp((arg), (long)) == 0))
#define store_value(into, argv, argi) \
    do { \
        if (!argv[argi + 1]) { dprintf(STDERR_FILENO, "Missing argument for option '%s'\n", argv[argi]); return 1; } \
        into = argv[++argi]; \
    } while (0)
#define option_error(message, ...) (dprintf(STDERR_FILENO, message "\n", ##__VA_ARGS__), 1)

int main(int argc, char *argv[], char *envp[])
{
    /* Parse arguments */
    if (argc <= 1)
        return usage(1, false);

    int argi = 1;
    struct options opts;
    memset(&opts, 0, sizeof(opts));
    opts.output_fd = STDOUT_FILENO;
    opts.drop_capabilities = true;

    for (; argi < argc; ++argi) {
        if (argv[argi][0] != '-' || strcmp(argv[argi], "--") == 0)
            break; /* End of options at -- or any non-prefixed argument */
        else if (option(argv[argi], "-h", "--help"))
            return usage(0, true);
        else if (option(argv[argi], "-e", "--exec"))
            store_value(opts.command_hook, argv, argi);
        else if (option(argv[argi], "-p", "--privileged"))
            opts.drop_capabilities = false;
        else if (option(argv[argi], "-s", "--split"))
            opts.split_outputs = true;
        else if (option(argv[argi], "-o", "--output"))
            store_value(opts.output_path, argv, argi);
        else if (option(argv[argi], NULL, "--internal"))
            opts.show_internal = true;
        else
            return option_error("Unknown option: '%s'", argv[argi]);
    }

    if (opts.split_outputs && !opts.output_path)
        return option_error("Cannot generate split outputs (-s) without an output path (-o)");
    if (opts.output_path && !opts.split_outputs)
        if ((opts.output_fd = open(opts.output_path, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
            die("Failed to open output file: %m");


    /* Spawn the child */
    int child = fork();
    if (child == -1)
        die("Failed to fork child process: %m");

    /* Dispatch */
    if (child == 0)
        child_tracee(&opts, &argv[argi], envp);
    else
        parent_tracer(child, &opts);
}
