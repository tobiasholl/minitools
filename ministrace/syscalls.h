/* ************************************ SYSCALL ARGUMENTS ************************************ */

/*
 * You may use static buffers to construct these, as long as you make sure they are valid
 * while printing the entire system call.
 * TODO: Allow disabling this for a more compact build.
 */

#define describe_constant(constant) case constant: return #constant; /* Make sure this isn't substituted too much */

#define add(buffer, index, join, string) \
    do { \
        size_t _remaining = sizeof(buffer) - 1 - index; \
        size_t _required = strlen(string) + ((index == 0) ? 0 : strlen(join)); \
        if (_required >= _remaining) \
            return "<buffer too small>"; \
        if (index != 0) { \
            memcpy(&buffer[index], join, strlen(join)); \
            index += strlen(join); \
        } \
        memcpy(&buffer[index], string, strlen(string)); \
        index += strlen(string); \
    } while (0)
#define add_bit(buffer, index, string) add(buffer, index, " | ", string)

#define check_bits(arg, mask, value, buffer, index, string) \
    do { \
        if ((arg & (mask)) == (value)) { \
            add_bit(buffer, index, string); \
            arg &= ~(mask); \
        } \
    } while (0)
#define check_bit(arg, value, buffer, index, string) \
    check_bits(arg, value, value, buffer, index, string)
#define add_bit_remainder(arg, buffer, index) \
    do { \
        size_t _remaining = sizeof(buffer) - 1 - index; \
        if ((size_t) snprintf(&buffer[index], _remaining, " | %#lx") >= _remaining) \
            return "<buffer too small>"; \
    } while (0)

static const char *label_memory_prot(unsigned long argument)
{
    switch (argument) {
        case 0: return "PROT_NONE";
        case 1: return "PROT_EXEC";
        case 2: return "PROT_WRITE";
        case 3: return "PROT_WRITE | PROT_EXEC";
        case 4: return "PROT_READ";
        case 5: return "PROT_READ | PROT_EXEC";
        case 6: return "PROT_READ | PROT_WRITE";
        case 7: return "PROT_READ | PROT_WRITE | PROT_EXEC";
        default: return "unknown";
    }
}

static const char *label_mmap_flags(unsigned long argument)
{
    static char buffer[1024];
    size_t index = 0;

    switch (argument & 0x03) {
        case 0x00: return "invalid";
        case 0x01: add_bit(buffer, index, "MAP_SHARED"); break;
        case 0x02: add_bit(buffer, index, "MAP_PRIVATE"); break;
        case 0x03: add_bit(buffer, index, "MAP_SHARED_VALIDATE"); break;
    }
    argument = argument & ~0x03ul;

    bool has_hugetlb = argument & 0x40000;

    check_bit(argument, 0x00000010, buffer, index, "MAP_FIXED");
    check_bit(argument, 0x00000020, buffer, index, "MAP_ANONYMOUS");
    check_bit(argument, 0x00000040, buffer, index, "MAP_32BIT");
    check_bit(argument, 0x00000100, buffer, index, "MAP_GROWSDOWN");
    check_bit(argument, 0x00000800, buffer, index, "MAP_DENYWRITE");
    check_bit(argument, 0x00001000, buffer, index, "MAP_EXECUTABLE");
    check_bit(argument, 0x00002000, buffer, index, "MAP_LOCKED");
    check_bit(argument, 0x00004000, buffer, index, "MAP_NORESERVE");
    check_bit(argument, 0x00008000, buffer, index, "MAP_POPULATE");
    check_bit(argument, 0x00010000, buffer, index, "MAP_NONBLOCK");
    check_bit(argument, 0x00020000, buffer, index, "MAP_STACK");
    check_bit(argument, 0x00040000, buffer, index, "MAP_HUGETLB");
    check_bit(argument, 0x00080000, buffer, index, "MAP_SYNC");
    check_bit(argument, 0x00100000, buffer, index, "MAP_FIXED_NOREPLACE");

    if (has_hugetlb) {
        check_bits(argument, 0xfc000000, 14 << 26, buffer, index, "MAP_HUGE_16KB");
        check_bits(argument, 0xfc000000, 16 << 26, buffer, index, "MAP_HUGE_64KB");
        check_bits(argument, 0xfc000000, 19 << 26, buffer, index, "MAP_HUGE_512KB");
        check_bits(argument, 0xfc000000, 20 << 26, buffer, index, "MAP_HUGE_1MB");
        check_bits(argument, 0xfc000000, 21 << 26, buffer, index, "MAP_HUGE_2MB");
        check_bits(argument, 0xfc000000, 23 << 26, buffer, index, "MAP_HUGE_8MB");
        check_bits(argument, 0xfc000000, 24 << 26, buffer, index, "MAP_HUGE_16MB");
        check_bits(argument, 0xfc000000, 25 << 26, buffer, index, "MAP_HUGE_32MB");
        check_bits(argument, 0xfc000000, 28 << 26, buffer, index, "MAP_HUGE_256MB");
        check_bits(argument, 0xfc000000, 29 << 26, buffer, index, "MAP_HUGE_512MB");
        check_bits(argument, 0xfc000000, 30 << 26, buffer, index, "MAP_HUGE_1GB");
        check_bits(argument, 0xfc000000, 31 << 26, buffer, index, "MAP_HUGE_2GB");
        check_bits(argument, 0xfc000000, 34 << 26, buffer, index, "MAP_HUGE_16GB");
    } else {
        check_bit(argument, 0x04000000, buffer, index, "MAP_UNINITIALIZED");
    }

    add_bit_remainder(argument, buffer, index);

    buffer[index] = 0;
    return &buffer[0];
}

static const char *label_access_mode(unsigned long argument)
{
    switch (argument) {
        case 0: return "F_OK";
        case 1: return "X_OK";
        case 2: return "W_OK";
        case 3: return "W_OK | X_OK";
        case 4: return "R_OK";
        case 5: return "R_OK | X_OK";
        case 6: return "R_OK | W_OK";
        case 7: return "R_OK | W_OK | X_OK";
        default: return "unknown";
    }
}

static const char *label_ptrace_request(unsigned long argument)
{
    switch (argument) {
        describe_constant(PTRACE_TRACEME);
        describe_constant(PTRACE_PEEKDATA);
        describe_constant(PTRACE_PEEKTEXT);
        describe_constant(PTRACE_PEEKUSER);
        describe_constant(PTRACE_POKEDATA);
        describe_constant(PTRACE_POKETEXT);
        describe_constant(PTRACE_POKEUSER);
        describe_constant(PTRACE_GETREGS);
        describe_constant(PTRACE_GETFPREGS);
        describe_constant(PTRACE_GETREGSET);
        describe_constant(PTRACE_SETREGS);
        describe_constant(PTRACE_SETFPREGS);
        describe_constant(PTRACE_SETREGSET);
        describe_constant(PTRACE_GETSIGINFO);
        describe_constant(PTRACE_SETSIGINFO);
        describe_constant(PTRACE_PEEKSIGINFO);
        describe_constant(PTRACE_GETSIGMASK);
        describe_constant(PTRACE_SETSIGMASK);
        describe_constant(PTRACE_SETOPTIONS);
        describe_constant(PTRACE_GETEVENTMSG);
        describe_constant(PTRACE_CONT);
        describe_constant(PTRACE_SYSCALL);
        describe_constant(PTRACE_SINGLESTEP);
#ifdef PTRACE_SET_SYSCALL /* apparently not defined on x86_64 */
        describe_constant(PTRACE_SET_SYSCALL);
#endif
        describe_constant(PTRACE_SYSEMU);
        describe_constant(PTRACE_SYSEMU_SINGLESTEP);
        describe_constant(PTRACE_LISTEN);
        describe_constant(PTRACE_KILL);
        describe_constant(PTRACE_INTERRUPT);
        describe_constant(PTRACE_ATTACH);
        describe_constant(PTRACE_SEIZE);
        describe_constant(PTRACE_SECCOMP_GET_FILTER);
        describe_constant(PTRACE_DETACH);
        describe_constant(PTRACE_GET_THREAD_AREA);
        describe_constant(PTRACE_SET_THREAD_AREA);
        describe_constant(PTRACE_GET_SYSCALL_INFO);
        default: return "unknown";
    }
}

static const char *label_openat_flags(unsigned long argument)
{
    static char buffer[1024];
    size_t index = 0;

    switch (argument & 0x03) {
        case 0x00: add_bit(buffer, index, "O_RDONLY"); break;
        case 0x01: add_bit(buffer, index, "O_WRONLY"); break;
        case 0x02: add_bit(buffer, index, "O_RDWR"); break;
        case 0x03: add_bit(buffer, index, "(O_RDWR+1)"); break; /* This is treated as O_RDWR */
    }
    argument = argument & ~0x03ul;

    check_bit(argument, 0x00000040, buffer, index, "O_CREAT");
    check_bit(argument, 0x00000080, buffer, index, "O_EXCL");
    check_bit(argument, 0x00000100, buffer, index, "O_NOCTTY");
    check_bit(argument, 0x00000200, buffer, index, "O_TRUNC");
    check_bit(argument, 0x00000400, buffer, index, "O_APPEND");
    check_bit(argument, 0x00000800, buffer, index, "O_NONBLOCK");
    /* O_DSYNC: see below */
    check_bit(argument, 0x00002000, buffer, index, "O_ASYNC"); /* == FASYNC */
    check_bit(argument, 0x00004000, buffer, index, "O_DIRECT");
    check_bit(argument, 0x00008000, buffer, index, "O_LARGEFILE");
    check_bit(argument, 0x00010000, buffer, index, "O_DIRECTORY");
    check_bit(argument, 0x00020000, buffer, index, "O_NOFOLLOW");
    check_bit(argument, 0x00040000, buffer, index, "O_NOATIME");
    check_bit(argument, 0x00080000, buffer, index, "O_CLOEXEC");
    /* O_SYNC / __O_SYNC: see below */
    check_bit(argument, 0x00200000, buffer, index, "O_PATH");
    check_bit(argument, 0x00400000, buffer, index, "__O_TMPFILE");

    /* These combine to form O_SYNC */
    check_bits(argument, 0x00101000, 0x00001000, buffer, index, "O_DSYNC");
    check_bits(argument, 0x00101000, 0x00100000, buffer, index, "__O_SYNC");
    check_bits(argument, 0x00101000, 0x00101000, buffer, index, "O_SYNC");

    add_bit_remainder(argument, buffer, index);

    buffer[index] = 0;
    return &buffer[0];
}

static const char *label_at_flags(unsigned long argument)
{
    static char buffer[1024];
    size_t index = 0;

    check_bit(argument, 0x0100, buffer, index, "AT_SYMLINK_NOFOLLOW");
    check_bit(argument, 0x0200, buffer, index, "AT_EACCESS/AT_REMOVEDIR");
    check_bit(argument, 0x0400, buffer, index, "AT_SYMLINK_FOLLOW");
    check_bit(argument, 0x0800, buffer, index, "AT_NO_AUTOMOUNT");
    check_bit(argument, 0x1000, buffer, index, "AT_EMPTY_PATH");
    check_bit(argument, 0x2000, buffer, index, "AT_STATX_FORCE_SYNC");
    check_bit(argument, 0x4000, buffer, index, "AT_STATX_DONT_SYNC");
    check_bit(argument, 0x8000, buffer, index, "AT_RECURSIVE");
    add_bit_remainder(argument, buffer, index);

    buffer[index] = 0;
    return &buffer[0];
}


/* ************************************ SYSCALL DESCRIPTIONS ************************************ */

#define _(arg) arg
#define __syscall_args(notif, a, b, c, d, e, f, ...) \
    a((notif)->data.args[0]), b((notif)->data.args[1]), c((notif)->data.args[2]), \
    d((notif)->data.args[3]), e((notif)->data.args[4]), f((notif)->data.args[5])
#define __syscall_default_format "%#lx, %#lx, %#lx, %#lx, %#lx, %#lx"

#define __syscall_case(fd, notif, name, fmt, ...) \
    do { \
        dprintf(fd, "[pid %d] %#lx: " name "(" fmt ")\n", (notif)->pid, (notif)->data.instruction_pointer, __syscall_args(notif, ##__VA_ARGS__, _, _, _, _, _, _)); \
    } while (0)
#define syscall_case_explicit(fd, notif, name, fmt, ...) \
    case concat(__NR_, name): \
        __syscall_case(fd, notif, stringify(name), fmt, ##__VA_ARGS__); \
        break

#define __with_arg_spec(arg) (arg), arg
#define with_arg(labeler) labeler __with_arg_spec
#define __prepend_arg(arg) arg
#define prepend_arg(extra) extra, __prepend_arg

#define describe_unknown_syscall(fd, notif) \
    __syscall_case(fd, notif, "syscall_%ld", __syscall_default_format, prepend_arg((notif)->data.nr))


static void describe_syscall(int fd, struct seccomp_notif *notif)
{
#define syscall_case(name, ...) syscall_case_explicit(fd, notif, name, __VA_ARGS__)
#define syscall_default_case(name) syscall_case_explicit(fd, notif, name, __syscall_default_format)
    /*
     * TODO: Add new syscalls, and provide proper argument specifications for the rest.
     * TODO: Deep argument inspection for strings and structs.
     */
    switch (notif->data.nr) {
        syscall_case(read, "%ld, %#lx, %#lx");
        syscall_case(write, "%ld, %#lx, %#lx");
        syscall_case(open, "%#lx, %#lx, %#lo");
        syscall_case(close, "%ld");
        syscall_default_case(stat);
        syscall_default_case(fstat);
        syscall_default_case(lstat);
        syscall_default_case(poll);
        syscall_default_case(lseek);
        syscall_case(mmap, "%#lx, %#lx, %s (%ld), %s (%#lx), %d, %ld", _, _, with_arg(label_memory_prot), with_arg(label_mmap_flags));
        syscall_case(mprotect, "%#lx, %#lx, %s (%d)", _, _, with_arg(label_memory_prot));
        syscall_case(munmap, "%#lx, %#lx");
        syscall_case(brk, "%#lx");
        syscall_default_case(rt_sigaction);
        syscall_default_case(rt_sigprocmask);
        syscall_default_case(rt_sigreturn);
        syscall_case(ioctl, "%d, %#lx, %#lx");
        syscall_case(pread64, "%d, %#lx, %#lx, %ld");
        syscall_case(pwrite64, "%d, %#lx, %#lx, %ld");
        syscall_case(readv, "%d, %#lx, %#lx");
        syscall_case(writev, "%d, %#lx, %#lx");
        syscall_case(access, "%#lx, %s (%#lx)", _, with_arg(label_access_mode));
        syscall_default_case(pipe);
        syscall_default_case(select);
        syscall_default_case(sched_yield);
        syscall_default_case(mremap);
        syscall_default_case(msync);
        syscall_default_case(mincore);
        syscall_default_case(madvise);
        syscall_default_case(shmget);
        syscall_default_case(shmat);
        syscall_default_case(shmctl);
        syscall_default_case(dup);
        syscall_default_case(dup2);
        syscall_default_case(pause);
        syscall_default_case(nanosleep);
        syscall_default_case(getitimer);
        syscall_default_case(alarm);
        syscall_default_case(setitimer);
        syscall_case(getpid, "");
        syscall_default_case(sendfile);
        syscall_default_case(socket);
        syscall_default_case(connect);
        syscall_default_case(accept);
        syscall_default_case(sendto);
        syscall_default_case(recvfrom);
        syscall_default_case(sendmsg);
        syscall_default_case(recvmsg);
        syscall_default_case(shutdown);
        syscall_default_case(bind);
        syscall_default_case(listen);
        syscall_default_case(getsockname);
        syscall_default_case(getpeername);
        syscall_default_case(socketpair);
        syscall_default_case(setsockopt);
        syscall_default_case(getsockopt);
        syscall_default_case(clone);
        syscall_default_case(fork);
        syscall_default_case(vfork);
        syscall_case(execve, "%#lx, %#lx, %#lx");
        syscall_case(exit, "%ld");
        syscall_default_case(wait4);
        syscall_default_case(kill);
        syscall_default_case(uname);
        syscall_default_case(semget);
        syscall_default_case(semop);
        syscall_default_case(semctl);
        syscall_default_case(shmdt);
        syscall_default_case(msgget);
        syscall_default_case(msgsnd);
        syscall_default_case(msgrcv);
        syscall_default_case(msgctl);
        syscall_default_case(fcntl);
        syscall_default_case(flock);
        syscall_default_case(fsync);
        syscall_default_case(fdatasync);
        syscall_default_case(truncate);
        syscall_default_case(ftruncate);
        syscall_default_case(getdents);
        syscall_default_case(getcwd);
        syscall_default_case(chdir);
        syscall_default_case(fchdir);
        syscall_default_case(rename);
        syscall_default_case(mkdir);
        syscall_default_case(rmdir);
        syscall_default_case(creat);
        syscall_default_case(link);
        syscall_default_case(unlink);
        syscall_default_case(symlink);
        syscall_default_case(readlink);
        syscall_default_case(chmod);
        syscall_default_case(fchmod);
        syscall_default_case(chown);
        syscall_default_case(fchown);
        syscall_default_case(lchown);
        syscall_default_case(umask);
        syscall_default_case(gettimeofday);
        syscall_default_case(getrlimit);
        syscall_default_case(getrusage);
        syscall_default_case(sysinfo);
        syscall_default_case(times);
        syscall_case(ptrace, "%s (%ld), %ld, %#lx, %#lx", with_arg(label_ptrace_request));
        syscall_case(getuid, "");
        syscall_default_case(syslog);
        syscall_case(getgid, "");
        syscall_default_case(setuid);
        syscall_default_case(setgid);
        syscall_default_case(geteuid);
        syscall_default_case(getegid);
        syscall_default_case(setpgid);
        syscall_default_case(getppid);
        syscall_default_case(getpgrp);
        syscall_default_case(setsid);
        syscall_default_case(setreuid);
        syscall_default_case(setregid);
        syscall_default_case(getgroups);
        syscall_default_case(setgroups);
        syscall_default_case(setresuid);
        syscall_default_case(getresuid);
        syscall_default_case(setresgid);
        syscall_default_case(getresgid);
        syscall_default_case(getpgid);
        syscall_default_case(setfsuid);
        syscall_default_case(setfsgid);
        syscall_default_case(getsid);
        syscall_default_case(capget);
        syscall_default_case(capset);
        syscall_default_case(rt_sigpending);
        syscall_default_case(rt_sigtimedwait);
        syscall_default_case(rt_sigqueueinfo);
        syscall_default_case(rt_sigsuspend);
        syscall_default_case(sigaltstack);
        syscall_default_case(utime);
        syscall_default_case(mknod);
        syscall_default_case(uselib);
        syscall_default_case(personality);
        syscall_default_case(ustat);
        syscall_default_case(statfs);
        syscall_default_case(fstatfs);
        syscall_default_case(sysfs);
        syscall_default_case(getpriority);
        syscall_default_case(setpriority);
        syscall_default_case(sched_setparam);
        syscall_default_case(sched_getparam);
        syscall_default_case(sched_setscheduler);
        syscall_default_case(sched_getscheduler);
        syscall_default_case(sched_get_priority_max);
        syscall_default_case(sched_get_priority_min);
        syscall_default_case(sched_rr_get_interval);
        syscall_default_case(mlock);
        syscall_default_case(munlock);
        syscall_default_case(mlockall);
        syscall_default_case(munlockall);
        syscall_default_case(vhangup);
        syscall_default_case(modify_ldt);
        syscall_default_case(pivot_root);
        syscall_default_case(_sysctl);
        syscall_default_case(prctl);
        syscall_case(arch_prctl, "%#lx, %#lx"); /* TODO: Flags */
        syscall_default_case(adjtimex);
        syscall_default_case(setrlimit);
        syscall_default_case(chroot);
        syscall_default_case(sync);
        syscall_default_case(acct);
        syscall_default_case(settimeofday);
        syscall_default_case(mount);
        syscall_default_case(umount2);
        syscall_default_case(swapon);
        syscall_default_case(swapoff);
        syscall_default_case(reboot);
        syscall_default_case(sethostname);
        syscall_default_case(setdomainname);
        syscall_default_case(iopl);
        syscall_default_case(ioperm);
        syscall_default_case(create_module);
        syscall_default_case(init_module);
        syscall_default_case(delete_module);
        syscall_default_case(get_kernel_syms);
        syscall_default_case(query_module);
        syscall_default_case(quotactl);
        syscall_default_case(nfsservctl);
        syscall_default_case(getpmsg);
        syscall_case(gettid, "");
        syscall_default_case(readahead);
        syscall_default_case(setxattr);
        syscall_default_case(lsetxattr);
        syscall_default_case(fsetxattr);
        syscall_default_case(getxattr);
        syscall_default_case(lgetxattr);
        syscall_default_case(fgetxattr);
        syscall_default_case(listxattr);
        syscall_default_case(llistxattr);
        syscall_default_case(flistxattr);
        syscall_default_case(removexattr);
        syscall_default_case(lremovexattr);
        syscall_default_case(fremovexattr);
        syscall_default_case(tkill);
        syscall_default_case(time);
        syscall_default_case(futex);
        syscall_default_case(sched_setaffinity);
        syscall_default_case(sched_getaffinity);
        syscall_default_case(set_thread_area);
        syscall_default_case(io_setup);
        syscall_default_case(io_destroy);
        syscall_default_case(io_getevents);
        syscall_default_case(io_submit);
        syscall_default_case(io_cancel);
        syscall_default_case(get_thread_area);
        syscall_default_case(lookup_dcookie);
        syscall_default_case(epoll_create);
        syscall_default_case(epoll_ctl_old);
        syscall_default_case(epoll_wait_old);
        syscall_default_case(remap_file_pages);
        syscall_default_case(getdents64);
        syscall_default_case(set_tid_address);
        syscall_default_case(restart_syscall);
        syscall_default_case(semtimedop);
        syscall_default_case(fadvise64);
        syscall_default_case(timer_create);
        syscall_default_case(timer_settime);
        syscall_default_case(timer_gettime);
        syscall_default_case(timer_getoverrun);
        syscall_default_case(timer_delete);
        syscall_default_case(clock_settime);
        syscall_default_case(clock_gettime);
        syscall_default_case(clock_getres);
        syscall_default_case(clock_nanosleep);
        syscall_case(exit_group, "%ld");
        syscall_default_case(epoll_wait);
        syscall_default_case(epoll_ctl);
        syscall_default_case(tgkill);
        syscall_default_case(utimes);
        syscall_default_case(mbind);
        syscall_default_case(set_mempolicy);
        syscall_default_case(get_mempolicy);
        syscall_default_case(mq_open);
        syscall_default_case(mq_unlink);
        syscall_default_case(mq_timedsend);
        syscall_default_case(mq_timedreceive);
        syscall_default_case(mq_notify);
        syscall_default_case(mq_getsetattr);
        syscall_default_case(kexec_load);
        syscall_default_case(waitid);
        syscall_default_case(add_key);
        syscall_default_case(request_key);
        syscall_default_case(keyctl);
        syscall_default_case(ioprio_set);
        syscall_default_case(ioprio_get);
        syscall_default_case(inotify_init);
        syscall_default_case(inotify_add_watch);
        syscall_default_case(inotify_rm_watch);
        syscall_default_case(migrate_pages);
        syscall_case(openat, "%d, %#lx, %s (%#lx), %#o", _, _, with_arg(label_openat_flags));
        syscall_default_case(mkdirat);
        syscall_default_case(mknodat);
        syscall_default_case(fchownat);
        syscall_default_case(futimesat);
        syscall_case(newfstatat, "%d, %#lx, %#lx, %s (%#lx)", _, _, _, with_arg(label_at_flags));
        syscall_default_case(unlinkat);
        syscall_default_case(renameat);
        syscall_default_case(linkat);
        syscall_default_case(symlinkat);
        syscall_default_case(readlinkat);
        syscall_default_case(fchmodat);
        syscall_default_case(faccessat);
        syscall_default_case(pselect6);
        syscall_default_case(ppoll);
        syscall_default_case(unshare);
        syscall_default_case(set_robust_list);
        syscall_default_case(get_robust_list);
        syscall_default_case(splice);
        syscall_default_case(tee);
        syscall_default_case(sync_file_range);
        syscall_default_case(vmsplice);
        syscall_default_case(move_pages);
        syscall_default_case(utimensat);
        syscall_default_case(epoll_pwait);
        syscall_default_case(signalfd);
        syscall_default_case(timerfd_create);
        syscall_default_case(eventfd);
        syscall_default_case(fallocate);
        syscall_default_case(timerfd_settime);
        syscall_default_case(timerfd_gettime);
        syscall_default_case(accept4);
        syscall_default_case(signalfd4);
        syscall_default_case(eventfd2);
        syscall_default_case(epoll_create1);
        syscall_default_case(dup3);
        syscall_default_case(pipe2);
        syscall_default_case(inotify_init1);
        syscall_case(preadv, "%d, %#lx, %#lx, %ld");
        syscall_case(pwritev, "%d, %#lx, %#lx, %ld");
        syscall_default_case(rt_tgsigqueueinfo);
        syscall_default_case(perf_event_open);
        syscall_default_case(recvmmsg);
        syscall_default_case(fanotify_init);
        syscall_default_case(fanotify_mark);
        syscall_default_case(prlimit64);
        syscall_default_case(name_to_handle_at);
        syscall_default_case(open_by_handle_at);
        syscall_default_case(clock_adjtime);
        syscall_default_case(syncfs);
        syscall_default_case(sendmmsg);
        syscall_default_case(setns);
        syscall_default_case(getcpu);
        syscall_default_case(process_vm_readv);
        syscall_default_case(process_vm_writev);
        syscall_default_case(kcmp);
        syscall_default_case(finit_module);
        syscall_default_case(sched_setattr);
        syscall_default_case(sched_getattr);
        syscall_default_case(renameat2);
        syscall_default_case(seccomp);
        syscall_default_case(getrandom);
        syscall_default_case(memfd_create);
        syscall_default_case(kexec_file_load);
        syscall_default_case(bpf);
        syscall_default_case(execveat);
        syscall_default_case(userfaultfd);
        syscall_default_case(membarrier);
        syscall_default_case(mlock2);
        syscall_default_case(copy_file_range);
        syscall_default_case(preadv2);
        syscall_default_case(pwritev2);
        syscall_default_case(pkey_mprotect);
        syscall_default_case(pkey_alloc);
        syscall_default_case(pkey_free);
        syscall_default_case(statx);
        syscall_default_case(io_pgetevents);
        syscall_default_case(rseq);
        syscall_default_case(pidfd_send_signal);
        syscall_default_case(io_uring_setup);
        syscall_default_case(io_uring_enter);
        syscall_default_case(io_uring_register);
        syscall_default_case(open_tree);
        syscall_default_case(move_mount);
        syscall_default_case(fsopen);
        syscall_default_case(fsconfig);
        syscall_default_case(fsmount);
        syscall_default_case(fspick);
        syscall_default_case(pidfd_open);
        syscall_default_case(clone3);
        syscall_default_case(close_range);
        syscall_default_case(openat2);
        syscall_default_case(pidfd_getfd);
        syscall_default_case(faccessat2);
        syscall_default_case(process_madvise);
        syscall_default_case(epoll_pwait2);
        syscall_default_case(mount_setattr);
        syscall_default_case(quotactl_fd);
        syscall_default_case(landlock_create_ruleset);
        syscall_default_case(landlock_add_rule);
        syscall_default_case(landlock_restrict_self);
        syscall_default_case(memfd_secret);
        syscall_default_case(process_mrelease);
        syscall_default_case(futex_waitv);
        syscall_default_case(set_mempolicy_home_node);
        default:
            describe_unknown_syscall(fd, notif);
    }
}
