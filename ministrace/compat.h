#include <stdint.h>

#define SECCOMP_RET_USER_NOTIF 0x7fc00000u
#define SECCOMP_FILTER_FLAG_NEW_LISTENER (1ul << 3)
#define SECCOMP_GET_NOTIF_SIZES 3
#define SECCOMP_USER_NOTIF_FLAG_CONTINUE (1UL << 0)

struct seccomp_notif {
    uint64_t id;
    uint32_t pid;
    uint32_t flags;
    struct seccomp_data data;
};

struct seccomp_notif_sizes {
    uint16_t seccomp_notif;
    uint16_t seccomp_notif_resp;
    uint16_t seccomp_data;
};

struct seccomp_notif_resp {
    uint64_t id;
    int64_t val;
    int32_t error;
    uint32_t flags;
};

#define SECCOMP_IOC_MAGIC '!'
#define SECCOMP_IOCTL_NOTIF_RECV     _IOWR(SECCOMP_IOC_MAGIC, 0, struct seccomp_notif)
#define SECCOMP_IOCTL_NOTIF_SEND     _IOWR(SECCOMP_IOC_MAGIC, 1, struct seccomp_notif_resp)
#define SECCOMP_IOCTL_NOTIF_ID_VALID _IOW(SECCOMP_IOC_MAGIC,  2, uint64_t)

/*
 * I don't use those except in the big switch() in syscalls.h, where them being #defined would
 * cause chaos.
 */
#undef pread64
#undef pwrite64
#undef prlimit64

#if defined(__x86_64__)
#define __X32_SYSCALL_BIT 0x40000000
#define __NR_pread64 17
#define __NR_pwrite64 18
#define __NR_prlimit64 302
#define __NR_quotactl_fd 443
#define __NR_memfd_secret 447
#define __NR_process_mrelease 448
#define __NR_futex_waitv 449
#define __NR_set_mempolicy_home_node 450
#else
#error Please update system call numbers in compat.h
#endif
