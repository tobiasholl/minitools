CC = clang
CFLAGS = -O3 -Wall -Wextra -Wformat -Werror=format-security -fstack-protector-all -D_FORTIFY_SOURCE=3 -static

flags_miniitrace := -lcapstone
flags_ministrace := -Wno-format

# Here begins the magic machinery that builds all the tools for us.
.DEFAULT_GOAL := all
MAKEFILE := $(realpath $(lastword $(MAKEFILE_LIST)))
MAKEPATH := $(realpath $(dir $(MAKEFILE)))
TOOLS := $(foreach toolpath,$(wildcard $(MAKEPATH)/mini*),$(notdir $(toolpath)))

# add-rule (name, dependencies, commmands)
define add-rule =
$(1): $(2)
	$(3)

endef

# info-eval (text)
ifeq ($(MAKE_DEBUG),1)
define info-eval =
$(info $(1))
$(eval $(1))
endef
else
define info-eval =
$(eval $(1))
endef
endif

# tool-rule (name, dependencies, directory)
ifeq ($(MAKELEVEL), 0)
define tool-rule =
$(call info-eval,$(call add-rule,$(1),$(foreach dep,$(2),$(3)/$(dep)),$$(MAKE) -f $(MAKEFILE) -C $(3) $(1)))
$(call info-eval,$(call add-rule,clean-$(1),,$$(MAKE) -f $(MAKEFILE) -C $(3) clean-$(1)))
endef
else
define tool-rule =
$(call info-eval,$(call add-rule,$(1),$(2),$$(CC) $$(CFLAGS) $$(filter %.c,$$^) $$(flags_$(1)) -o $$@))
$(call info-eval,$(call add-rule,clean-$(1),,$$(RM) $(1)))
endef
endif

define glob-source-filenames =
$(filter-out _%,$(foreach dep,$(wildcard $(1)/*.c) $(wildcard $(1)/*.h),$(notdir $(dep))))
endef

define generate-tool-rules =
$(foreach tool,$(TOOLS),$(call tool-rule,$(tool),$(call glob-source-filenames,$(MAKEPATH)/$(tool)),$(tool)))
endef

$(call generate-tool-rules)

all: $(TOOLS)
.PHONY: all

CLEAN_RULES := $(patsubst %,clean-%,$(TOOLS))
clean: $(CLEAN_RULES)
.PHONY: clean $(CLEAN_RULES)

# On the top level, all tools are phony. Below, not so much
ifeq ($(MAKELEVEL), 0)
.PHONY: $(TOOLS)
endif
