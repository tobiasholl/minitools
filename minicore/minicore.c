#define _GNU_SOURCE
#define _LARGEFILE64_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define die(fmt, ...) do { dprintf(STDERR_FILENO, fmt "\n", ##__VA_ARGS__); exit(-1); } while (0)

int pid = 0; // Global because musl-gcc doesn't have on_exit

void unsuspend(void)
{
    if (pid && kill(pid, SIGCONT))
        dprintf(STDERR_FILENO, "Failed to resume PID %d, please run kill -CONT %d\n", pid, pid);
}

int main(int argc, char *argv[])
{
    if (argc < 3)
        die("Usage: %s [-S] PID COREFILE", argv[0]);

    struct options {
        int suspend;
    } opts = { .suspend = 1 };

    int argi = 1;
    if (strcmp(argv[argi], "--no-suspend") == 0 || strcmp(argv[argi], "-S") == 0) {
        opts.suspend = 0;
        ++argi;
    }
    if (argv[argi][0] == '-')
        die("Unknown option '%s'", argv[argi]);

    const char *pid_string = argv[argi++];
    const char *corefile = argv[argi++];
    if (argi < argc)
        die("Unknown argument '%s'", argv[argi]);

    errno = 0;
    char *end = NULL;
    pid = strtol(pid_string, &end, 10);
    if (errno != 0)
        die("Invalid PID '%s': %m", pid_string);
    else if (end != pid_string + strlen(pid_string))
        die("Could not convert '%s' as part of the PID", end);

    int proc_fd = open("/proc", O_PATH | O_CLOEXEC);
    if (proc_fd == -1)
        die("Failed to open /proc: %m");

    if (opts.suspend) {
        if (kill(pid, SIGSTOP))
            die("Failed to suspend target: %m");
        atexit(unsuspend);
    }

    int pid_dir_fd = openat(proc_fd, pid_string, O_PATH | O_CLOEXEC);
    if (pid_dir_fd == -1)
        die("Failed to open /proc/%d: %m", pid);
    close(proc_fd);

    int maps_fd = openat(pid_dir_fd, "maps", O_RDONLY | O_CLOEXEC);
    if (maps_fd == -1)
        die("Failed to open /proc/%d/maps: %m", pid);

    int mem_fd = openat(pid_dir_fd, "mem", O_RDONLY | O_CLOEXEC);
    if (mem_fd == -1)
        die("Failed to open /proc/%d/mem: %m", pid);
    close(pid_dir_fd);

    int out_fd = open(corefile, O_CREAT | O_TRUNC | O_RDWR | O_CLOEXEC, 0600);
    if (out_fd == -1)
        die("Failed to open output file %s: %m", corefile);

    char line_buffer[512];
#define line_capacity (sizeof(line_buffer) - 1)
    size_t remaining = line_capacity;
    for (;;) {
        struct {
            off64_t start;
            off64_t end;
        } section;
        char read_perm;

        // Read until we have enough to get start pointer, end pointer, and formatting
        char *head = &line_buffer[line_capacity - remaining];
        while (sscanf(line_buffer, "%lx-%lx %c", (unsigned long *) &section.start, (unsigned long *) &section.end, &read_perm) < 3) {
            if (!remaining)
                die("Line beginning is too long. This should not happen. Dumping line.\n%s", line_buffer);

            ssize_t bytes = read(maps_fd, head, remaining);
            if (bytes < 0)
                die("Failed to read from /proc/%d/maps: %m", pid);
            else if (bytes == 0 && head == &line_buffer[0])
                return 0; // All data read.
            else if (bytes == 0)
                die("Premature end of file in /proc/%d/maps", pid);

            head += bytes;
            remaining -= bytes;
            *head = '\0'; // String operations should always work. This is never past-the-end since remaining is 1 byte less at most.
        }
        dprintf(STDERR_FILENO, "%#018lx - %#018lx%s\n", section.start, section.end, read_perm == 'r' ? "" : " (not readable)");

        int eio = 0;
        if (read_perm == 'r') {
            // First read some data, then dump the header with the first write
            int wrote_header = 0;
            // We can't copy_file_range because EXDEV. We can't mmap because procfs is annoying. So do it the old-fashioned way.
            if (lseek64(mem_fd, section.start, SEEK_SET) == (off64_t) -1)
                die("Failed to seek to offset %lx: %m", section.start);
            while (section.start < section.end) {
                char copy_buffer[4096];
                ssize_t reads = read(mem_fd, copy_buffer, sizeof(copy_buffer));
                if (reads < 0) {
                    if (errno == EIO && !wrote_header) {
                        eio = 1;
                        break;
                    }
                    die("Failed to read data at offset %lx: %m", section.start);
                } else if (reads == 0)
                    die("Failed to read data at offset %lx: No data available", section.start);

                if (!wrote_header) {
                    ssize_t written = write(out_fd, &section, sizeof(section));
                    if (written < 0)
                        die("Failed to write to output file: %m");
                    else if (written != sizeof(section))
                        die("Unhandled partial write of output file");
                    wrote_header = 1;
                }
                section.start += reads;


                char *buffer_start = copy_buffer;
                while (reads) {
                    ssize_t written = write(out_fd, buffer_start, reads);
                    if (written < 0)
                        die("Failed to write data at offset %lx to the output file: %m", section.start - reads);
                    else if (written == 0)
                        die("Failed to write data at offset %lx to the output file.", section.start - reads);
                    reads -= written;
                    buffer_start += written;
                }
            }
        }

        char *newline;
        if (eio) {
            // This should be [vvar], [vdso], or [vsyscall]
            // Read carefully until we hit a newline.
            while (!(newline = strchr(line_buffer, '\n'))) {
                if (!remaining)
                    die("Line is too long - probably not one of the \"good\" mappings to EIO.\n    %s", line_buffer);

                ssize_t bytes = read(maps_fd, head, remaining);
                if (bytes < 0)
                    die("Failed to read from /proc/%d/maps: %m", pid);
                else if (bytes == 0)
                    die("Premature end of file in /proc/%d/maps", pid);

                head += bytes;
                remaining -= bytes;
                *head = '\0'; // String operations should always work. This is never past-the-end since remaining is 1 byte less at most.
            }

            if (!strstr(line_buffer, "[vvar]\n") && strstr(line_buffer, "[vdso]\n") && strstr(line_buffer, "[vsyscall]\n"))
                die("Failed to read data at offset %lx: %s", section.start, strerror(EIO));
        }

        // Look for a newline
        while (!(newline = strchr(line_buffer, '\n'))) {
            // Discard the entire buffer and read some data.
            ssize_t bytes = read(maps_fd, line_buffer, sizeof(line_buffer) - 1);
            if (bytes < 0)
                die("Failed to read from /proc/%d/maps: %m", pid);
            else if (bytes == 0)
                die("Premature end of file in /proc/%d/maps", pid);
            line_buffer[bytes] = '\0';
        }
        size_t after = strlen(newline);
        memmove(line_buffer, newline + 1, after);
        remaining = line_capacity - after + 1; // + 0 byte
    }
}
