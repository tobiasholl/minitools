#!/usr/bin/python
import argparse
import struct
import sys

any_int = lambda i: int(i, 0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='The minicore dump to parse')
    ops = parser.add_mutually_exclusive_group(required=True)
    ops.add_argument('--only', help='Dump only the section containing this address', type=any_int, default=None)
    ops.add_argument('--range', help='Dump exactly this range', type=any_int, nargs=2, metavar=('FROM', 'TO'))
    args = parser.parse_args()

    with open(args.path, 'rb') as corefile:
        while header := corefile.read(16):
            start, end = struct.unpack('QQ', header)
            assert start < end, f'Invalid section: start ({start:#x}) >= end ({end:#x})'
            section = corefile.read(end - start)
            if args.only is not None:
                if start <= args.only and args.only < end:
                    sys.stdout.buffer.write(section)
                    break
            elif args.range is not None:
                assert args.range[0] < args.range[1], f'Invalid range requested: start ({args.range[0]:#x}) >= end ({args.range[1]:#x})'
                if args.range[0] >= end:
                    continue # Starts after this section
                if args.range[1] <= start:
                    break # Already done
                if args.range[0] <= start and end <= args.range[1]:
                    # Full section requested
                    sys.stdout.buffer.write(section)
                elif args.range[0] <= start and args.range[1] < end:
                    # Must remove some off the end
                    sys.stdout.buffer.write(section[:args.range[1] - start])
                    break # Done!
                elif start < args.range[0] and end <= args.range[1]:
                    # Must remove some off the start
                    sys.stdout.buffer.write(section[args.range[0] - start:])
                else:
                    # Must remove both ends
                    sys.stdout.buffer.write(section[args.range[0] - start:args.range[1] - start])
                    break # Done!
