/*
 * A tiny nsjail replacement for environments that doesn't have external dependencies.
 * This should build statically (-static or -static-pie) with either musl or glibc.
 * This doesn't do any flexible configuration, resource limiting, or networking right now.
 */

#define _GNU_SOURCE
#include <fcntl.h>
#include <grp.h>
#include <limits.h>
#include <linux/audit.h>
#include <linux/capability.h>
#include <linux/filter.h>
#include <linux/sched.h>
#include <linux/seccomp.h>
#include <linux/securebits.h>
#include <sched.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/prctl.h>
#include <sys/socket.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <unistd.h>


/* ************************************ CONFIGURATION ************************************ */

/* Which namespaces to unshare (default: all of them) */
#define MINIJAIL_NAMESPACES (CLONE_NEWNS | CLONE_NEWCGROUP | CLONE_NEWUTS | CLONE_NEWIPC | CLONE_NEWUSER | CLONE_NEWPID | CLONE_NEWNET)

/* UID/GID mappings: Each line here is <starting id inside> <starting id outside> <length of range>. By default, map root inside the container to an unprivileged user on the outside. */
#define MINIJAIL_UID_MAP "0 1000 1\n2000 2000 1"
#define MINIJAIL_GID_MAP "0 1000 1\n2000 2000 1"

/* Which UID/GID to actually use inside the container (the _inside_ IDs from above) */
#define MINIJAIL_UID 2000
#define MINIJAIL_GID 2000

/* Hostname */
#define MINIJAIL_HOSTNAME "minijail"

/* Which directory contains this container's root file system */
#define MINIJAIL_CHROOT "/jail"

/* Where and how to mount procfs */
#define MINIJAIL_PROC_MOUNT "/proc"
#define MINIJAIL_PROC_OPTIONS "hidepid=2"

/* Where and how to mount tmpfs */
#define MINIJAIL_TMP_MOUNT "/tmp"
#define MINIJAIL_TMP_OPTIONS ""

/* Bind mounts (see struct bind_mount below) */
#define MINIJAIL_BIND_MOUNTS { \
    { .src = "/dev/one", .dst = "/dev/one" }, \
    { .src = "/dev/two", .dst = "/dev/two" }, \
    { .src = "/sys", .dst = "/sys" }, \
}

/* Seccomp filter (in BPF instructions, see seccomp(2) on how to write those). */
#define MINIJAIL_SECCOMP_BPF { \
    /* Check the architecture */ \
    /* 00 */ BPF_STMT(BPF_LD  | BPF_ABS | BPF_W, (offsetof(struct seccomp_data, arch))), \
    /* 01 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, AUDIT_ARCH_X86_64,     0, 23), \
    /* Check against prohibited system calls */ \
    /* 02 */ BPF_STMT(BPF_LD  | BPF_ABS | BPF_W, (offsetof(struct seccomp_data, nr))), \
    /* 03 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_bpf,             21, 0), \
    /* 04 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_clone,           21, 0), /* special handling */ \
    /* 05 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_clone3,          19, 0), /* LWN 822256 */ \
    /* 06 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_delete_module,   18, 0), \
    /* 07 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_finit_module,    17, 0), \
    /* 08 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_fsconfig,        16, 0), \
    /* 09 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_fsopen,          15, 0), \
    /* 10 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_fspick,          14, 0), \
    /* 11 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_init_module,     13, 0), \
    /* 12 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_iopl,            12, 0), \
    /* 13 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_ioperm,          11, 0), \
    /* 14 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_kexec_file_load, 10, 0), \
    /* 15 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_kexec_load,       9, 0), \
    /* 16 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_modify_ldt,       8, 0), \
    /* 17 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_mount,            7, 0), \
    /* 18 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_move_mount,       6, 0), \
    /* 19 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_pivot_root,       5, 0), \
    /* 20 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_setns,            4, 0), \
    /* 21 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_unshare,          3, 0), \
    /* 22 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, __NR_userfaultfd,      2, 0), \
    /* ...and against the maximum system call number that I sanity checked (this prohibits x32 compatibility syscalls also) */ \
    /* 23 */ BPF_JUMP(BPF_JMP | BPF_JGE | BPF_K, 451,                   1, 0), \
    /* Allow anything else, but kill the process on disallowed syscalls */ \
    /* 24 */ BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ALLOW), \
    /* 25 */ BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_KILL_PROCESS), \
    /* clone: Check args[0] (only block namespaces that we already unshare) */ \
    /* 26 */ BPF_STMT(BPF_LD  | BPF_ABS | BPF_W, (offsetof(struct seccomp_data, args[0]))), \
    /* 27 */ BPF_STMT(BPF_ALU | BPF_AND | BPF_K, (MINIJAIL_NAMESPACES & 0xffffffffu)), \
    /* 28 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, 0, 0, 3), \
    /* 29 */ BPF_STMT(BPF_LD  | BPF_ABS | BPF_W, (offsetof(struct seccomp_data, args[0]) + 4)), \
    /* 30 */ BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, 0, 0, 1), \
    /* 31 */ BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ALLOW), \
    /* 32 */ BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_KILL_PROCESS), \
}


/* ************************************ DATA TYPES ************************************ */

struct bind_mount { const char *src; const char *dst; int read_only; };

/* <linux/sched.h> is terribly outdated on musl. */
#if !__has_include(<sys/queue.h>) && !defined(MINIJAIL_BUILD_HAS_CLONE_ARGS)
    #define CLONE_CLEAR_SIGHAND 0x100000000ULL
    struct clone_args {
        uint64_t flags;
        uint64_t pidfd;
        uint64_t child_tid;
        uint64_t parent_tid;
        uint64_t exit_signal;
        uint64_t stack;
        uint64_t stack_size;
        uint64_t tls;
        uint64_t set_tid;
        uint64_t set_tid_size;
        uint64_t cgroup;
    };
#endif
#if __has_include(<linux/close_range.h>)
#include <linux/close_range.h>
#else
#define close_range(...) syscall(__NR_close_range, ##__VA_ARGS__)
#define CLOSE_RANGE_CLOEXEC (1u << 2)
#endif


/* ************************************ UTILITIES ************************************ */

#define __stringify(arg) #arg
#define stringify(arg) __stringify(arg)
#define die(fmt, ...) do { dprintf(STDERR_FILENO, __FILE__ ":" stringify(__LINE__) " | " fmt "\n", ##__VA_ARGS__); exit(1); } while (0)
#define array_size(arr) (sizeof(arr) / sizeof(arr[0]))

static inline void proc_write(int dirfd, const char *path, const char *buffer, int pid, const char *what)
{
    int fd = openat(dirfd, path, O_WRONLY);
    if (fd == -1)
        die("Failed to open /proc/%d/%s: %m", pid, path);
    ssize_t bytes = write(fd, buffer, strlen(buffer));
    if (bytes < 0)
        die("Failed to write %s to /proc/%d/%s: %m", what, pid, path);
    else if ((size_t) bytes != strlen(buffer))
        die("Failed to write %s to /proc/%d/%s: Incomplete write", what, pid, path);
    close(fd);
}

static inline const char *path_cat(const char *parent, const char *child)
{
    static char concatenated[PATH_MAX];
    if ((size_t) snprintf(concatenated, sizeof(concatenated), "%s/%s", parent, child) >= sizeof(concatenated))
        die("Path too long: %s/%s\n", parent, child);
    return concatenated;
}

static inline void send_sync(int comm)
{
    if (send(comm, "S", 1, MSG_EOR) != 1)
        die("Failed to send synchronization message: %m");
}

static inline void wait_for_sync(int comm)
{
    char sync;
    if (read(comm, &sync, 1) != 1)
        die("Failed to wait for synchronization message: %m");
}


/* ************************************ MINIJAIL ************************************ */

const struct bind_mount bind_mounts[] = MINIJAIL_BIND_MOUNTS;
struct sock_filter seccomp_filter[] = MINIJAIL_SECCOMP_BPF;

int unshared_child(int comm, char *argv[])
{
    /* Switch UID and GID once the mapping is ready. */
    wait_for_sync(comm);
    if (prctl(PR_SET_SECUREBITS, SECBIT_NO_SETUID_FIXUP | SECBIT_KEEP_CAPS, 0, 0, 0))
        die("Failed to set prctl secure bits (see capabilities(7)): %m");
    if (setresgid(MINIJAIL_GID, MINIJAIL_GID, MINIJAIL_GID))
        die("Failed to set GID: %m");
    if (setresuid(MINIJAIL_UID, MINIJAIL_UID, MINIJAIL_UID))
        die("Failed to set UID: %m");
    if (prctl(PR_SET_SECUREBITS, 0, 0, 0, 0))
        die("Failed to set prctl secure bits (see capabilities(7)): %m");

    /* Detach from normal mount tree */
    if (mount("/", "/", NULL, MS_REC | MS_PRIVATE, NULL))
        die("Failed to make mounts private: %m");

    /* Set up the root file system */
    char temporary_root[] = "/tmp/minijail.XXXXXX";
    if (mkdtemp(temporary_root) == NULL)
        die("Failed to create root mount point: %m");
    if (mount(MINIJAIL_CHROOT, temporary_root, NULL, MS_BIND | MS_REC | MS_PRIVATE, NULL))
        die("Failed to mount root: %m");
    if (chdir(temporary_root))
        die("Failed to change directory: %m");

    /* Mount /proc and all other specified bind mounts */
    if (MINIJAIL_PROC_MOUNT)
        if (mount("proc", path_cat(temporary_root, MINIJAIL_PROC_MOUNT), "proc", 0, MINIJAIL_PROC_OPTIONS))
            die("Failed to mount proc to " MINIJAIL_PROC_MOUNT ": %m");
    if (MINIJAIL_TMP_MOUNT)
        if (mount("none", path_cat(temporary_root, MINIJAIL_TMP_MOUNT), "tmpfs", 0, MINIJAIL_TMP_OPTIONS))
            die("Failed to mount tmpfs to " MINIJAIL_TMP_MOUNT ": %m");

    for (size_t i = 0; i < array_size(bind_mounts); ++i)
        if (access(bind_mounts[i].src, F_OK) == 0)
            if (mount(bind_mounts[i].src, path_cat(temporary_root, bind_mounts[i].dst), NULL, MS_BIND | MS_REC | MS_PRIVATE | (bind_mounts[i].read_only ? MS_RDONLY : 0), NULL))
                die("Failed to bind-mount %s to %s: %m", bind_mounts[i].src, bind_mounts[i].dst);

    /* chroot (without pivot_root - this is designed to work on initramfs) */
    if (mount(".", "/", NULL, MS_MOVE, NULL))
        die("Failed to move root mount to /: %m");
    if (chroot("."))
        die("Failed to chroot: %m");

    /* Set hostname */
    if (sethostname(MINIJAIL_HOSTNAME, strlen(MINIJAIL_HOSTNAME)))
        die("Failed to set hostname: %m");

    /* Die on parent exit */
    prctl(PR_SET_PDEATHSIG, SIGKILL, 0, 0, 0);

    /* Drop any remaining capabilities */
    struct __user_cap_header_struct cap_header = { .version = _LINUX_CAPABILITY_VERSION_3, .pid = 0 };
    struct __user_cap_data_struct cap_data[2];
    if (syscall(__NR_capget, &cap_header, &cap_data[0]))
        die("Failed to query current capabilities: %m");
    for (size_t i = 0; i < array_size(cap_data); ++i) {
        cap_data[i].permitted = cap_data[i].effective;
        cap_data[i].inheritable = 0;
    }
    if (syscall(__NR_capset, &cap_header, &cap_data[0]))
        die("Failed to drop capabilities: %m");

    if (prctl(PR_CAP_AMBIENT, PR_CAP_AMBIENT_CLEAR_ALL, 0, 0, 0))
        die("Failed to clear ambient capability set: %m");
    for (size_t capability = 0; capability <= CAP_LAST_CAP; ++capability)
        if (prctl(PR_CAPBSET_DROP, capability))
            die("Failed to drop bounding capability %zu: %m", capability);

    /* Add seccomp filter */
    if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0))
        die("Failed to set no-new-privileges mode: %m");
    struct sock_fprog filter = {
        .len = array_size(seccomp_filter),
        .filter = seccomp_filter
    };
    if (prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, &filter, 0, 0))
        die("Failed to install seccomp filter: %m");
    if (close(comm))
        die("Failed to close communications file descriptor: %m");

    /* Close all remaining file descriptors that are not stdio. musl doesn't have the wrapper. */
    if (close_range(STDERR_FILENO + 1, ~0u, CLOSE_RANGE_CLOEXEC))
        die("Failed to mark remaining file descriptors close-on-exec: %m");

    execve(argv[1], &argv[1], NULL);
    die("Failed to launch %s: %m", argv[1]);
}

int parent(int comm, int child)
{
    /* Get a handle to /proc/<child>/ */
    char proc_pid_path[NAME_MAX];
    if ((size_t) snprintf(proc_pid_path, sizeof(proc_pid_path), "/proc/%d/", child) >= sizeof(proc_pid_path))
        die("Buffer too small for /proc/%d/", child);

    int proc_pid = open(proc_pid_path, O_PATH);
    if (proc_pid == -1)
        die("Failed to open /proc/%d/: %m", child);

    /* Set up UID/GID mapping in the container */
    proc_write(proc_pid, "uid_map",   MINIJAIL_UID_MAP, child, "UID mapping");
    proc_write(proc_pid, "setgroups", "deny",           child, "'deny'");
    proc_write(proc_pid, "gid_map",   MINIJAIL_GID_MAP, child, "GID mapping");
    send_sync(comm);
    close(comm);

    if (waitpid(child, NULL, 0) != child)
        die("Failed to wait for child: %m");
    return 0;
}

void reap_child(int signo)
{
    (void) signo;
    for (int pid; (pid = waitpid((pid_t) -1, NULL, WNOHANG)) > 0;) { /* Do nothing - waitpid() is enough. */ }
}

#define PARENT_COMM 0
#define CHILD_COMM 1
int main(int argc, char *argv[])
{
    /* Simple usage check */
    if (argc <= 1)
        die("Usage: %s command...", argv[0]);

    /* Communication channel between child and parent */
    int comms[2];
    if (socketpair(AF_UNIX, SOCK_SEQPACKET, 0, comms))
        die("Failed to create communication sockets: %m");

    /* SIGCHLD handling */
    signal(SIGCHLD, reap_child);

    /* Leave supplementary groups before clone() to avoid leaking groups. We can't do this in the namespace (see user_namespaces(7)) */
    if (setgroups(0, NULL))
        die("Failed to leave supplementary groups: %m");

    /* Clone into new namespaces */
    struct clone_args clone_args = {
        .flags = MINIJAIL_NAMESPACES | CLONE_CLEAR_SIGHAND,
        .exit_signal = SIGCHLD,
    };
    int child = syscall(__NR_clone3, &clone_args, sizeof(clone_args));
    if (child == -1)
        die("Failed to clone child process: %m");

    /* Close the other socket, then dispatch */
    close(comms[child == 0 ? PARENT_COMM : CHILD_COMM]);
    if (child == 0)
        return unshared_child(comms[CHILD_COMM], argv);
    else
        return parent(comms[PARENT_COMM], child);
}
